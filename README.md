Installation
------------

 0. SHELL$   `git clone https://github.com/tkych/cl-enigma.git`
 1. CL-REPL> `(push #p"/path-to-cl-enigma/cl-enigma/" asdf:*central-registry*)`
 2. CL-REPL> `(ql:quickload :cl-enigma)` or `(asdf:load-system :cl-enigma)`


Examples
--------

```lisp
ENIGMA> (machine B
                 (set-rotor Beta  #\G)
                 (set-rotor Gamma #\B)
                 (set-rotor VIII  #\D)
                 '((a y) (l x))
                 "SUPERCALIFRAGILISTICEXPIALIDOCIOUS")
=> "NPFJDLEGBRBUVNZVFXSJHTXUMUGQSZGUWG"

ENIGMA> (machine B
                 (set-rotor Beta  #\G)
                 (set-rotor Gamma #\B)
                 (set-rotor VIII  #\D)
                 '((a y) (l x))
                 *)
=> "SUPERCALIFRAGILISTICEXPIALIDOCIOUS"
```


Referece Manual
---------------

#### [Function] MACHINE _reflector_ _left-rotor_ _middle-rotor_ _right-rotor_ _plugs_ _input-string_ => _encoded/decoded-string_

Encode/Decode _input-string_.

Argumants:

 * _reflector_ is a one of reflectors (built-in reflector A,B,C, or user defined reflector).
 * _left-rotor_, _middle-rotor_, _right-rotor_ are a one of rotors (built-in rotor I,II,III,IV,V,VI,VII,VIII,Beta,Gamma or user defined rotor).
 * _plugs_ is a list of a two characters/symbols which interchanges each other.
e.g. _plugs_, `((#\a #\b) (c d))` makes plugboard a <-> b and c <-> d.
 * _input-string_ is a string to be encode/decode by machine.


#### [Function] SET-ROTOR _rotor_ _init-position_ => _rotor_

Set bar-position of the _rotor_ to _init-position_.
_init-position_ must be a character (a...z) or an integer (1...26).


#### [Function] MAKE-ROTOR _to-alphabet_ _&rest_ _notch-characters_ => _rotor_

Make a rotor.

_to-alphabet_ is a permutation of \*ALPHABET\*.
_notch-characters_ are a characters which replesents the position of turnover.

e.g.
`(make-rotor "BACDEFGHIJKLMNOPQRSTUVWXYZ" #\N #\W)`
makes rotor which interchanges `A <-> B` and has notchs for `#\N` and `#\W`.


#### [Special Variables] I, II, III, IV, V, VII, VIII, Beta, Gamma

Built-in rotors.

Definitions:

 * I     := `(make-rotor "EKMFLGDQVZNTOWYHXUSPAIBRCJ" #\Q)`
 * II    := `(make-rotor "AJDKSIRUXBLHWTMCQGZNPYFVOE" #\E)`
 * III   := `(make-rotor "BDFHJLCPRTXVZNYEIWGAKMUSQO" #\V)`
 * IV    := `(make-rotor "ESOVPZJAYQUIRHXLNFTGKDCMWB" #\J)`
 * V     := `(make-rotor "VZBRGITYUPSDNHLXAWMJQOFECK" #\Z)`
 * VI    := `(make-rotor "JPGVOUMFYQBENHZRDKASXLICTW" #\Z #\M)`
 * VII   := `(make-rotor "NZJHGRCXMYSWBOUFAIVLPEKQDT" #\Z #\M)`
 * VIII  := `(make-rotor "FKQHTLXOCBJSPDZRAMEWNIUYGV" #\Z #\M)`
 * Beta  := `(make-rotor "LEYJVCNIXWPBQMDRTAKZGFUHOS")`
 * Gamma := `(make-rotor "FSOKANUERHMBTIYCWLQPZXVGJD")`


#### [Function] MAKE-REFLECTOR _to-alphabet_ => _reflector_

Make a reflector.

_to-alphabet_ is permutation of \*ALPHABET\*.


#### [Special Variables] A, B, C

Built-in reflectors.

Definitions:

 * A := `(make-reflector "EJMZALYXVBWFCRQUONTSPIKHGD")`
 * B := `(make-reflector "YRUHQSLDPXNGOKMIEBFZCWVJAT")`
 * C := `(make-reflector "FVPJIAOYEDRZXWGCTKUQSBNMHL")`


#### [Special Variable] \*ALPHABET\*

The alphabet string used in a plaintext and a cyphertext.
Default is `"ABCDEFGHIJKLMNOPQRSTUVWXYZ"`.
