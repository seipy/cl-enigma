;; cl-enigma/cl-enigma.asd


;;====================================================================
;; CL-ENIGMA: 
;;====================================================================
;; cl-enigma/
;;   cl-enigma.asd
;;   enigma.lisp
;;   README.md
;;   LICENSE
;;   CHANGELOG


;;====================================================================
;; System for CL-ENIGMA
;;====================================================================

(asdf:defsystem #:cl-enigma
  :name        "cl-enigma"
  :description "Enigma Machine Simulator for Common Lisp."
  :version     "0.1.00"
  :licence     "GNU General Public License version 3 or later"
  :author      "Ali Reza Hayati" 
  :components  ((:file "enigma"))
  )

;;====================================================================
